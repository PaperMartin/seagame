float3 GerstnerWave (float4 wave, float3 p, inout float3 tangent, inout float3 binormal) {
	float steepness = wave.z;
	float wavelength = wave.w;
	float k = 2 * PI / wavelength;
	float c = sqrt(9.8 / k);
	float2 d = normalize(wave.xy);
	float f = k * (dot(d, p.xz) - c * _Time.y);
	float a = steepness / k;
			
	//p.x += d.x * (a * cos(f));
	//p.y = a * sin(f);
	//p.z += d.y * (a * cos(f));

	tangent += float3(
		1 -d.x * d.x * (steepness * sin(f)),
		d.x * (steepness * cos(f)),
		-d.x * d.y * (steepness * sin(f))
	);
	binormal += float3(
		-d.x * d.y * (steepness * sin(f)),
		d.y * (steepness * cos(f)),
		1 -d.y * d.y * (steepness * sin(f))
	);
	return float3(
		d.x * (a * cos(f)),
		a * sin(f),
		d.y * (a * cos(f))
	);
}


void TessendorfSim_float(float3 positionWS, float WaveLength, float WaveSteepness, float NormalCalcOffset, out float3 newPositionWS, out float3 newNormalWS)
{ 
	float4 Wave1 = float4(1,1 ,0.25 / 2,60 / 4);
	float4 Wave2 = float4(1,0.6, 0.25 / 2,31 / 4);
	float4 Wave3 = float4(1,1.2, 0.25 / 2,18 / 4);
	float4 Wave6 = float4(1.4,1, 0.25 / 2,18 / 4);
	float4 Wave4 = float4(0.4,1, 0.125 / 2,9 / 4);
	float4 Wave5 = float4(1,0.8, 0.125 / 2,9 / 4);
	
    float3 currentPos = positionWS;
    float3 tangent = 0;
    float3 binormal = 0;
    float3 p = currentPos;
    p += GerstnerWave(Wave1,currentPos,tangent,binormal);
	p += GerstnerWave(Wave2,currentPos,tangent,binormal);
	p += GerstnerWave(Wave3,currentPos,tangent,binormal);
	p += GerstnerWave(Wave4,currentPos,tangent,binormal);
	p += GerstnerWave(Wave5,currentPos,tangent,binormal);
	p += GerstnerWave(Wave6,currentPos,tangent,binormal);

	newPositionWS = p;
    newNormalWS = normalize(cross(binormal, tangent));
}