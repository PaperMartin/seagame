using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.PlayerLoop;
using UnityEngine.Rendering;
using UnityEditor;

public class WaterManager : MonoBehaviour
{
    [SerializeField] private WavesGenerator _deepWavesGenerator;
    [SerializeField] private WavesGenerator _shallowWavesGenerator;
    private static readonly int WaveOffset = Shader.PropertyToID("_WaveOffset");
    private static readonly int WaveDisplacement = Shader.PropertyToID("_WaveDisplacement");
    private static readonly int WaveDerivatives = Shader.PropertyToID("_WaveDerivatives");
    private static readonly int WaveTurbulence = Shader.PropertyToID("_WaveTurbulence");
    private static readonly int LengthScale0 = Shader.PropertyToID("_LengthScale0");


    private void Awake()
    {
        _deepWavesGenerator.InitializeSimulation();
        
    }

    private void Update()
    {
        _deepWavesGenerator.UpdateSimulation();
        WaveData deepWaveData = _deepWavesGenerator.GetWaveData();
        Shader.SetGlobalVector(WaveOffset,Vector4.zero);
        Shader.SetGlobalFloat(LengthScale0,deepWaveData.LengthScale);
        Shader.SetGlobalTexture(WaveDisplacement, deepWaveData.Displacement);
        Shader.SetGlobalTexture(WaveDerivatives, deepWaveData.Derivatives);
        Shader.SetGlobalTexture(WaveTurbulence, deepWaveData.Turbulence);
    }

}