using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Rendering;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class FFTWavesGenerator : WavesGenerator
{
    [SerializeField] private int SimulationSize = 256;
    [SerializeField] private float SimulationSpeed = 1f;
    [SerializeField] private FFTWavesSettings wavesSettings;
    [FormerlySerializedAs("LengthScale")] [SerializeField] private float lengthScale0 = 1000;
    
    [SerializeField]
    float lengthScale1 = 17;
    [SerializeField]
    float lengthScale2 = 5;

    [SerializeField] private bool alwaysUpdateInitialSpectrum = false;

    private Texture2D gaussianNoise;
    [SerializeField] private ComputeShader InitialSpectrumShader;
    private ComputeBuffer _paramsBuffer;
    private RenderTexture _H0K;
    private RenderTexture _wavesData;
    private RenderTexture _initialSpectrumRT;

    [SerializeField] private ComputeShader FourierComponentsShader;
    private RenderTexture Dx_Dz;
    private RenderTexture Dy_Dxz;
    private RenderTexture Dyx_Dyz;
    private RenderTexture Dxx_Dzz;

    [FormerlySerializedAs("ButterflyShader")] [SerializeField] private ComputeShader FFTShader;
    private RenderTexture _precomputedData;
    private RenderTexture _pingpong1;


    [FormerlySerializedAs("DisplacementShader")] [SerializeField] private ComputeShader WavesMerger;
    private RenderTexture _displacement;
    private RenderTexture _derivatives;
    private RenderTexture _turbulence;

    public override void InitializeSimulation()
    {
        InitializeTexturesAndBuffers();
        UpdateInitialSpectrum();
        PrecomputeTwiddleFactorsAndInputIndices();
    }

    #region GPUSide

    private void UpdateInitialSpectrum()
    {
        InitialSpectrumShader.SetInt("Size", SimulationSize);
        InitialSpectrumShader.SetFloat("LengthScale", lengthScale0);

        float boundary1 = 2 * Mathf.PI / lengthScale1 * 6f;
        InitialSpectrumShader.SetFloat("lowCutoff", 0.0001f);
        InitialSpectrumShader.SetFloat("highCutoff", boundary1);

        wavesSettings.SetParametersToShader(InitialSpectrumShader, 0, _paramsBuffer);

        InitialSpectrumShader.SetTexture(0, "H0K", _H0K);
        InitialSpectrumShader.SetTexture(0, "H0", _initialSpectrumRT);
        InitialSpectrumShader.SetTexture(0, "WavesData", _wavesData);
        InitialSpectrumShader.SetTexture(0, "gaussianNoise", gaussianNoise);

        InitialSpectrumShader.Dispatch(0, SimulationSize / 8, SimulationSize / 8, 1);

        wavesSettings.SetParametersToShader(InitialSpectrumShader, 1, _paramsBuffer);

        InitialSpectrumShader.SetTexture(1, "H0K", _H0K);
        InitialSpectrumShader.SetTexture(1, "H0", _initialSpectrumRT);

        InitialSpectrumShader.Dispatch(1, SimulationSize / 8, SimulationSize / 8, 1);
    }

    private void UpdateFourierComponents()
    {
        FourierComponentsShader.SetFloat("t", Time.time * SimulationSpeed);
        FourierComponentsShader.SetTexture(0, "Dx_Dz", Dx_Dz);
        FourierComponentsShader.SetTexture(0, "Dy_Dxz", Dy_Dxz);
        FourierComponentsShader.SetTexture(0, "Dyx_Dyz", Dyx_Dyz);
        FourierComponentsShader.SetTexture(0, "Dxx_Dzz", Dxx_Dzz);
        FourierComponentsShader.SetTexture(0, "H0", _initialSpectrumRT);
        FourierComponentsShader.SetTexture(0, "WavesData", _wavesData);
        FourierComponentsShader.Dispatch(0, SimulationSize / 8, SimulationSize / 8, 1);
    }

    private int BitReverse(int x, int numBits)
    {
        int reversed = 0;

        for (int i = 0; i < numBits; i++)
        {
            // If the ith bit of x is toggled, toggle the ith bit from the right of reversed
            reversed |= (x & (1 << i)) != 0 ? 1 << (numBits - 1 - i) : 0;
        }

        return reversed;
    }

    private void PrecomputeTwiddleFactorsAndInputIndices()
    {
        int logSize = (int)Mathf.Log(SimulationSize, 2);
        _precomputedData = new RenderTexture(logSize, SimulationSize, 0,
            RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.Linear);
        _precomputedData.filterMode = FilterMode.Point;
        _precomputedData.wrapMode = TextureWrapMode.Repeat;
        _precomputedData.enableRandomWrite = true;
        _precomputedData.Create();

        FFTShader.SetInt("Size", SimulationSize);
        FFTShader.SetTexture(4, "PrecomputeBuffer", _precomputedData);
        FFTShader.Dispatch(4, logSize, SimulationSize / 2 / 8, 1);
    }

    private void UpdateFFT()
    {
        PerformIFFT(Dx_Dz);
        PerformIFFT(Dy_Dxz);
        PerformIFFT(Dyx_Dyz);
        PerformIFFT(Dxx_Dzz);
    }

    private void PerformIFFT(RenderTexture component)
    {
        int logSize = (int)Mathf.Log(SimulationSize, 2);
        bool pingPong = false;

        FFTShader.SetTexture(0, "PrecomputedData", _precomputedData);
        FFTShader.SetTexture(0, "Buffer0", component);
        FFTShader.SetTexture(0, "Buffer1", _pingpong1);
        for (int i = 0; i < logSize; i++)
        {
            pingPong = !pingPong;
            FFTShader.SetInt("Step", i);
            FFTShader.SetBool("PingPong", pingPong);
            FFTShader.Dispatch(0, SimulationSize / 8,
                SimulationSize / 8, 1);
        }

        FFTShader.SetTexture(1, "PrecomputedData", _precomputedData);
        FFTShader.SetTexture(1, "Buffer0", component);
        FFTShader.SetTexture(1, "Buffer1", _pingpong1);
        for (int i = 0; i < logSize; i++)
        {
            pingPong = !pingPong;
            FFTShader.SetInt("Step", i);
            FFTShader.SetBool("PingPong", pingPong);
            FFTShader.Dispatch(1, SimulationSize / 8,
                SimulationSize / 8, 1);
        }

        if (pingPong)
        {
            Graphics.Blit(_pingpong1, component);
        }


        FFTShader.SetInt("Size", SimulationSize);
        FFTShader.SetTexture(3, "Buffer0", component);
        FFTShader.Dispatch(3, SimulationSize / 8, SimulationSize / 8, 1);
    }

    private void MergeDisplacement()
    {
        // Filling displacement and normals textures
        WavesMerger.SetFloat("DeltaTime", Time.deltaTime);

        WavesMerger.SetTexture(0, "Dx_Dz", Dx_Dz);
        WavesMerger.SetTexture(0, "Dy_Dxz", Dy_Dxz);
        WavesMerger.SetTexture(0, "Dyx_Dyz", Dyx_Dyz);
        WavesMerger.SetTexture(0, "Dxx_Dzz", Dxx_Dzz);
        WavesMerger.SetTexture(0, "Displacement", _displacement);
        WavesMerger.SetTexture(0, "Derivatives", _derivatives);
        WavesMerger.SetTexture(0, "Turbulence", _turbulence);
        WavesMerger.SetFloat("Lambda", wavesSettings.GetLambda());
        WavesMerger.SetFloat("DeltaTime", Time.deltaTime);
        WavesMerger.Dispatch(0, SimulationSize / 8, SimulationSize / 8, 1);
        
        _derivatives.GenerateMips();
        _turbulence.GenerateMips();
    }

    public override void UpdateSimulation()
    {
        Profiler.BeginSample("Update Ocean Simulation");
        if (alwaysUpdateInitialSpectrum)
        {
            UpdateInitialSpectrum();
        }

        UpdateFourierComponents();
        UpdateFFT();
        MergeDisplacement();
        Profiler.EndSample();
    }

    #endregion

    #region CPUSide

    private RenderTexture CreateTexture(int width, int height, RenderTextureFormat format, bool UseMipMaps = false)
    {
        RenderTexture RT = new RenderTexture(width, height, 0);
        RT.format = format;
        RT.antiAliasing = 1;
        RT.enableRandomWrite = true;
        RT.wrapMode = TextureWrapMode.Repeat;
        RT.autoGenerateMips = false;
        RT.useMipMap = UseMipMaps;
        RT.Create();

        return RT;
    }

    private void InitializeTexturesAndBuffers()
    {
        //Initial Spectrum
        _H0K = CreateTexture(SimulationSize, SimulationSize, RenderTextureFormat.ARGBFloat, false);
        _wavesData = CreateTexture(SimulationSize, SimulationSize, RenderTextureFormat.ARGBFloat,false);
        _initialSpectrumRT = CreateTexture(SimulationSize, SimulationSize, RenderTextureFormat.ARGBFloat,false);

        //fourier components
        Dx_Dz = CreateTexture(SimulationSize, SimulationSize, RenderTextureFormat.RGFloat,false);
        Dy_Dxz = CreateTexture(SimulationSize, SimulationSize, RenderTextureFormat.RGFloat,false);
        Dyx_Dyz = CreateTexture(SimulationSize, SimulationSize, RenderTextureFormat.RGFloat,false);
        Dxx_Dzz = CreateTexture(SimulationSize, SimulationSize, RenderTextureFormat.RGFloat,false);

        //pingpong texture
        _pingpong1 = CreateTexture(SimulationSize, SimulationSize, RenderTextureFormat.RGFloat,false);

        //displacement texture
        _displacement = CreateTexture(SimulationSize, SimulationSize, RenderTextureFormat.ARGBFloat,false);
        _derivatives = CreateTexture(SimulationSize, SimulationSize, RenderTextureFormat.ARGBFloat, true);
        _turbulence = CreateTexture(SimulationSize, SimulationSize, RenderTextureFormat.ARGBFloat, true);

        gaussianNoise = GetNoiseTexture(SimulationSize);

        _paramsBuffer = new ComputeBuffer(2, 8 * sizeof(float));
    }

    Texture2D GenerateNoiseTexture(int size, bool saveIntoAssetFile)
    {
        Texture2D noise = new Texture2D(size, size, TextureFormat.RGFloat, false, true);
        noise.filterMode = FilterMode.Point;
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                noise.SetPixel(i, j, new Vector4(NormalRandom(), NormalRandom()));
            }
        }

        noise.Apply();

#if UNITY_EDITOR
        if (saveIntoAssetFile)
        {
            string filename = "GaussianNoiseTexture" + size.ToString() + "x" + size.ToString();
            string path = "Assets/Resources/GaussianNoiseTextures/";
            AssetDatabase.CreateAsset(noise, path + filename + ".asset");
            Debug.Log("Texture \"" + filename + "\" was created at path \"" + path + "\".");
        }
#endif
        return noise;
    }

    float NormalRandom()
    {
        return Mathf.Cos(2 * Mathf.PI * Random.value) * Mathf.Sqrt(-2 * Mathf.Log(Random.value));
    }

    Texture2D GetNoiseTexture(int size)
    {
        string filename = "GaussianNoiseTexture" + size.ToString() + "x" + size.ToString();
        Texture2D noise = Resources.Load<Texture2D>("GaussianNoiseTextures/" + filename);
        return noise ? noise : GenerateNoiseTexture(size, true);
    }

    public override WaveData GetWaveData()
    {
        WaveData data = new WaveData();
        data.Displacement = _displacement;
        data.Derivatives = _derivatives;
        data.Turbulence = _turbulence;
        data.LengthScale = lengthScale0;
        return data;
    }


    public override float GetWaveHeightAtCoord(Vector2 Coords)
    {
        Coords *= Vector2.one / lengthScale0;

        return 0;
    }

    #endregion
}