using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct WaveData{
    public Texture Displacement;
    public Texture Derivatives;
    public Texture Turbulence;
    public float LengthScale;
}

public abstract class WavesGenerator : MonoBehaviour
{
    public abstract void InitializeSimulation();
    
    public abstract void UpdateSimulation();
    
    public abstract WaveData GetWaveData();
    
    public abstract float GetWaveHeightAtCoord(Vector2 Coords);
}
