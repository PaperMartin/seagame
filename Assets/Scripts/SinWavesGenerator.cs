using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class SinWavesGenerator : WavesGenerator
{
    [SerializeField] private int TextureSize = 256;
    [SerializeField] private int SinWaveNumber = 2;
    [SerializeField] private float SinWaveHeight = 5;
    [SerializeField] private Vector2 offset = Vector2.zero;
    [SerializeField] private Vector2 tiling = new Vector2(1, 1);
    [SerializeField] private Vector2 Speed = new Vector2(1, 1);
    [SerializeField] private ComputeShader sinWaveShader;

    [SerializeField] RenderTexture _sinWaveTexture;

    public override void InitializeSimulation()
    {
        CreateTexture();
    }

    private void CreateTexture()
    {
        _sinWaveTexture = new RenderTexture(TextureSize, TextureSize, 0, RenderTextureFormat.RFloat);
        _sinWaveTexture.filterMode = FilterMode.Bilinear;
        _sinWaveTexture.antiAliasing = 1;
        _sinWaveTexture.volumeDepth = 0;
        _sinWaveTexture.wrapMode = TextureWrapMode.Repeat;
        _sinWaveTexture.enableRandomWrite = true;
        _sinWaveTexture.Create();
    }

    private void OnDisable()
    {
        _sinWaveTexture.Release();
    }

    public override void UpdateSimulation()
    {
        sinWaveShader.SetFloat("WaveNumber", SinWaveNumber);
        sinWaveShader.SetVector("TextureSize", new Vector4(TextureSize, TextureSize, 0, 0));
        float time = Time.time;
#if UNITY_EDITOR
        time = Time.realtimeSinceStartup;
#endif
        sinWaveShader.SetVector("Offset", Speed * time);
        sinWaveShader.SetFloat("WaveHeight", SinWaveHeight);
        sinWaveShader.SetTexture(0, "Result", _sinWaveTexture);
        sinWaveShader.Dispatch(0, _sinWaveTexture.width / 8, _sinWaveTexture.height / 8, 1);
    }

    public override WaveData GetWaveData()
    {
        WaveData data = new WaveData();
        data.Displacement = _sinWaveTexture;
        return data;
    }

    public override float GetWaveHeightAtCoord(Vector2 Coords)
    {
        Vector2 currentCoords = Coords;
        currentCoords *= tiling;
        currentCoords += offset;
        float time = Time.time;
#if UNITY_EDITOR
        time = Time.realtimeSinceStartup;
#endif
        Vector2 TimeOffset = Speed * time;
        float resultx = Mathf.Sin((currentCoords.x * SinWaveNumber * Mathf.PI) + TimeOffset.x);
        float resulty = Mathf.Sin((currentCoords.y * SinWaveNumber * Mathf.PI) + TimeOffset.y);
        return resultx * resulty * SinWaveHeight;
    }
}