using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Management;

public class PlayerControllerSwitcher : MonoBehaviour
{
    [SerializeField] private bool IsVR;
    [SerializeField] private XRManagerSettings _settings;
    [SerializeField] private GameObject VRPlayer;
    [SerializeField] private GameObject DesktopPlayer;
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        if (IsVR)
        {
            _settings.InitializeLoaderSync();
            if (_settings.isInitializationComplete)
            {
                _settings.StartSubsystems();
                DesktopPlayer.SetActive(false);
                VRPlayer.SetActive(true);
                return;
            }
        }
        
        DesktopPlayer.SetActive(true);
        VRPlayer.SetActive(false);
    }

    private void OnDisable()
    {
        if (_settings.isInitializationComplete)
        {
            _settings.StopSubsystems();
        }
    }

    private void OnDestroy()
    {
        if (_settings.isInitializationComplete)
        {
            _settings.DeinitializeLoader();
        }
    }
}
